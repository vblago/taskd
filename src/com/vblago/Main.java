package com.vblago;

import java.util.Scanner;

public class Main {
    //корректная ли запись или это нужно делать в конструкторе?
    static Scanner in = new Scanner(System.in);
    static int quantity = Integer.parseInt(in.nextLine());
    static Books[] arrBooks = new Books[quantity];

    //пример ввода данных в readme

    public static void main(String[] args) {
        System.out.println("Введите данные о книгах:");
        getData();

        System.out.println("\nВыберите нужное действие:");
        System.out.println("1 - Найти самую старую книгу");
        System.out.println("2 - Найти книги автора");
        System.out.println("3 - Найти книги, изданные раннее введеного года");
        System.out.println("4 - Найти книги, написанные введеным автором в соавторстве");
        System.out.println("5 - Найти книги, где 3 и более авторов");
        System.out.println();

        int selection = -1;
        while (selection != 0) {
            selection = Integer.parseInt(in.nextLine());

            switch (selection) {
                case 1:
                    System.out.println("Автор(ы) самой старой книги: " + findAuthorOldestBook());
                    break;
                case 2:
                    //sout-ы писать в main или или в методах(возвращая какие-то значения)
                    //(какой код считается хорошим тоном?)
                    findNamesBooksOfAuthor();
                    break;
                case 3: findBooksOlderThen();
                    break;
                case 4: findBooksWithMoreThenThreeAuthors();
                    break;
                case 5: findInfBooksOfAuthor();
                    break;
                case 0: System.out.println("Вы завершили пользование программой");
                    break;
                default: System.out.println("Вы ввели неверное значение");
                    break;
            }
            System.out.println();
        }
    }

    static void getData() {
        for (int i = 0; i < quantity; i++) {
            Books bookObj = new Books();
            //почему нельзя напрямую обращатся к i элементу массива?
            // например: arrBooks[i].name = in.nextLine();
            //мы же создаем сначала экземпляр, а потом уже присваиваем его массиву
            bookObj.name = in.nextLine();
            bookObj.authors = in.nextLine().split(", ");
            bookObj.publishingHouse = in.nextLine();
            bookObj.year = Integer.parseInt(in.nextLine());
            arrBooks[i] = bookObj;
        }

    }

    static String findAuthorOldestBook() {
        int oldestBook = 0;
        for (int i = 1; i < quantity; i++) {
            if (arrBooks[oldestBook].year > arrBooks[i].year) {
                oldestBook = i;
            }
        }
        StringBuilder stringBuilder = new StringBuilder();
        for (String author : arrBooks[oldestBook].authors) {
            stringBuilder.append(author);
            stringBuilder.append("; ");
        }
        return stringBuilder.toString();
    }

    static void findNamesBooksOfAuthor() {
        System.out.print("Введите автора для вывода его книг: ");
        String author = in.nextLine();

        int counter = 0;
        for (Books arrBook : arrBooks) {
            for (String bookAuthor : arrBook.authors) {
                counter += (bookAuthor.equals(author)) ? 1 : 0;
            }
        }
        String[] books = new String[counter];
        int q = 0;
        for (Books arrBook : arrBooks) {
            for (String bookAuthor : arrBook.authors) {
                if (bookAuthor.equals(author)) {
                    books[q] = arrBook.name;
                    q++;
                }
            }
        }

        if (books.length != 0) {
            System.out.print("Книги данного автора: ");
            for (String booksNames : books) {
                System.out.print(booksNames + "; ");
            }
            System.out.println();
        } else {
            System.out.println("Книг данного автора нет");
        }
    }

    static void findBooksOlderThen() {
        System.out.print("Введите год, что-бы показать книги которые были изданы ранее: ");
        int date = Integer.parseInt(in.nextLine());
        int counter = 0;
        for (Books book : arrBooks) {
            counter += (book.year < date) ? 1 : 0;
        }
        String[] booksInf = new String[counter];
        int q = 0;
        for (int i = 0; i<arrBooks.length; i++) {
            if (arrBooks[i].year < date) {
                booksInf[q] = infAboutBook(i);
                q++;
            }
        }

        StringBuilder stringBuilder;
        if (booksInf.length != 0) {
            stringBuilder = new StringBuilder("Информация про книги кототые были изданы раннее ");
            stringBuilder.append(date);
            stringBuilder.append(" года:");
            System.out.println(stringBuilder.toString());
            for (String book : booksInf) {
                System.out.println(book);
            }
        } else {
            System.out.println("Книг данного автора нет");
        }
    }

    static void findBooksWithMoreThenThreeAuthors(){
        int counter = 0;
        for (Books arrBook : arrBooks) {
            if (arrBook.authors.length >= 3) {
                counter++;
            }
        }

        String[] booksInf = new String[counter];
        int q = 0;
        for (int i = 0; i<arrBooks.length; i++){
            if (arrBooks[i].authors.length >= 3){
                infAboutBook(i);
                booksInf[q] = infAboutBook(i);
                q++;
            }
        }
        System.out.println("Книги с 3 и более авторми: ");
        for (String books : booksInf){
            System.out.print(books+"; ");
        }
        System.out.println();
    }

    static void findInfBooksOfAuthor() {
        System.out.print("Введите автора для вывода его книг, написанных в соавторстве: ");
        String author = in.nextLine();

        int counter = 0;
        for (Books arrBook : arrBooks) {
            if (arrBook.authors.length > 1) {
                for (String bookAuthor : arrBook.authors) {
                    counter += (bookAuthor.equals(author)) ? 1 : 0;
                }
            }
        }

        String[] books = new String[counter];
        int q = 0;
        for (int i = 0; i<arrBooks.length; i++) {
            for (String bookAuthor : arrBooks[i].authors) {
                if (bookAuthor.equals(author)&&(arrBooks[i].authors.length > 1)) {
                    books[q] = infAboutBook(i);
                    q++;
                }
            }
        }

        if (books.length != 0) {
            System.out.println("Книги данного автора, написанные в соавторвстве: ");
            for (String booksInf : books) {
                System.out.println(booksInf);
            }
        } else {
            System.out.println("Книг данного автора, написанных в соавторстве, нет");
        }
    }

    static String infAboutBook(int num){
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(arrBooks[num].name);
        stringBuilder.append(" ");
        for (String author : arrBooks[num].authors){
            stringBuilder.append(author);
            stringBuilder.append("; ");
        }
        stringBuilder.append(" ");
        stringBuilder.append(arrBooks[num].publishingHouse);
        stringBuilder.append(" ");
        stringBuilder.append(arrBooks[num].year);
        return stringBuilder.toString();
    }

    static class Books {
        String name;
        String[] authors;
        int year;
        String publishingHouse;
    }
}
